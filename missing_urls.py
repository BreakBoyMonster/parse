import psycopg2
import logging
import datetime
from scraping.utils import *
from find_it.secret import DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, MAILGUN_KEY, API, ADMIN_EMAIL

today = datetime.date.today()

FROM_EMAIL = 'find.it.webcase@gmail.com'
SUBJECT = 'Missing urls {}'.format(today)

try:
    conn = psycopg2.connect(dbname=DB_NAME, host=DB_HOST, user=DB_USER, password=DB_PASSWORD)
except:
    logging.exception('Unable to open DataBase - {}'.format(today))
else:
    cur = conn.cursor()
    q = """SELECT city_id, speciality_id FROM subscribers_subscriber WHERE is_active='True';"""
    cur.execute(q)
    qs = cur.fetchall()
    cur.execute("""SELECT * FROM scraping_city;""")
    cities_qs = cur.fetchall()
    cities = {i[0]: i[1] for i in cities_qs}
    cur.execute("""SELECT * FROM scraping_speciality;""")
    sp_qs = cur.fetchall()
    sp = {i[0]: i[1] for i in sp_qs}
    mis_urls = []
    cnt = 'This date - {} there are missing pairs: '.format(today)
    for pair in qs:
        cur.execute("""SELECT * FROM scraping_url WHERE city_id=%s AND speciality_id=%s;""", (pair[0], pair[1]))
        qs = cur.fetchall()
        if not qs:
            mis_urls.append((cities[pair[0]], sp[pair[1]]))
    if mis_urls:
        for p in mis_urls:
            cnt += 'City - {}, speciality {}'.format(p[0], p[1])
        requests.post(API, auth=('api', MAILGUN_KEY),
                      data={'from': FROM_EMAIL, 'to': ADMIN_EMAIL, 'subject': SUBJECT, 'text': cnt})
    cur.execute("""SELECT data FROM scraping_error WHERE time_stamp=%s;""", (today,))
    err_qs = cur.fetchone()
    if err_qs:
        SUBJECT = 'Scraping errors on date {}'.format(today)
        data = err_qs[0]['errors']
        cnt = 'This date - {} there are missing pairs: '.format(today)
        for err in data:
            cnt += 'For url - {}, the reason is - {}\n'.format(err['href'], err['title'])
        requests.post(API, auth=('api', MAILGUN_KEY),
                      data={'from': FROM_EMAIL, 'to': ADMIN_EMAIL, 'subject': SUBJECT, 'text': cnt})


    conn.commit()
    cur.close()
    conn.close()
