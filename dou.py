import requests
from bs4 import BeautifulSoup as BS
# import codecs
import time
import re

# 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36}'
session = requests.session()
headers = {'User-Agent':
            'Mozilla/5.0 (Windows NT 5.1; rv:47.0) Gecko/20100101 Firefox/47.0',
           'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
           }
base_url = 'https://jobs.dou.ua/vacancies/?city=%D0%9E%D0%B4%D0%B5%D1%81%D1%81%D0%B0&category=Python'
jobs = []
pattern = r'\s{2,}|\xa0|\u2060'


def get_company(base_url):
    req = session.get(base_url, headers=headers)

    if req.status_code == 200:
        bsObj = BS(req.content, 'html.parser')
        div_list = bsObj.find_all('div', attrs={'class': 'vacancy'})
        for div in div_list:
            title = div.find('a', attrs={'class': 'vt'}).text
            href = div.find('a', attrs={'class': 'vt'})['href']
            company = "No company"
            short = "No short"
            a_company = div.find('a', attrs={'class': 'company'}).text
            if a_company:
                company = a_company
            a_short = div.find('div', attrs={'class': 'sh-info'}).text
            if a_short:
                short = a_short
                short = re.sub(pattern, ' ', short)
                short = short.strip()
            jobs.append({'title': title,
                         'href': href,
                         'description': short,
                         'company': company})
    return


get_company(base_url)

template = '<!doctype html><html lang="en">\n<head><meta charset="utf-8"></head>\n<body>'
end = '\n</body>\n</html>'
content = '\n<h2>Jobs.Dou.ua</h2>'
for job in jobs:
    content += '''\n\t<a href="{href}" target="_blank">{title}</a><br />
                  \n\t<p>{description}</p>\n\t<p>{company}</p>\n<br />'''.format(**job)
    content += '<hr><br /><br />'
data = template + content + end

handle = open('jobs.html', 'w', encoding='utf-8')
handle.write(str(data))
handle.close()

# for i in jobs:
#     print(i)
