import psycopg2
import logging
import datetime
from scraping.utils import *
from find_it.secret import DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, MAILGUN_KEY, API

today = datetime.date.today()
ten_days_ago = datetime.date.today() - datetime.timedelta(10)

FROM_EMAIL = 'find.it.webcase@gmail.com'
SUBJECT = 'Vacancies list for {}'.format(today)
template = '<!doctype html><html lang="en">\n<head><meta charset="utf-8"></head>\n<body>'
end = '\n</body>\n</html>'

try:
    conn = psycopg2.connect(dbname=DB_NAME, host=DB_HOST, user=DB_USER, password=DB_PASSWORD)
except:
    logging.exception('Unable to open DataBase - {}'.format(today))
else:
    # print('test')
    cur = conn.cursor()
    q = """SELECT city_id, speciality_id FROM subscribers_subscriber WHERE is_active='True';"""
    cur.execute(q)
    cities_qs = list(set(cur.fetchall()))
    for pair in cities_qs:
        content = ''
        city = pair[0]
        speciality = pair[1]
        cur.execute(
            """ SELECT email FROM subscribers_subscriber WHERE is_active=%s AND city_id=%s AND speciality_id=%s;""",
            (True, city, speciality))
        email_qs = cur.fetchall()
        emails = [i[0] for i in email_qs]
        cur.execute(
            """ SELECT url, title, description, company FROM scraping_vacancy WHERE city_id=%s AND speciality_id=%s AND time_stamp=%s;""",
            (city, speciality, today))
        jobs_qs = cur.fetchall()
        if jobs_qs:
            for job in jobs_qs:
                content += '\n\t<a href="{}" target="_blank">'.format(job[0])
                content += '{}</a><br />'.format(job[1])
                content += '\n\t<p>{}</p>'.format(job[2])
                content += '\n\t<p>{}</p>\n<br />'.format(job[3])
                content += '<hr><br /><br />'
            html_m = template + content + end
            for email in emails:
                # print(email)
                requests.post(API, auth=('api', MAILGUN_KEY),
                              data={'from': FROM_EMAIL, 'to': email, 'subject': SUBJECT, 'html': html_m})
        else:
            requests.post(API, auth=('api', MAILGUN_KEY),
                          data={'from': FROM_EMAIL, 'to': 'find.it.webcase@gmail.com', 'subject': SUBJECT,
                                'text': 'For today your profile vacancies list is empty'})
    conn.commit()
    cur.close()
    conn.close()
