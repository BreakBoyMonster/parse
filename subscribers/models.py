from django.db import models
from scraping.models import City, Speciality


class Subscriber(models.Model):
    email = models.CharField(max_length=100, unique=True, verbose_name='Email')
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='City')
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, verbose_name='Speciality')
    password = models.CharField(max_length=100, verbose_name='Password')
    is_active = models.BooleanField(default=True, verbose_name='Get mailing?')

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Subscriber'
        verbose_name_plural = 'Subscribers'
