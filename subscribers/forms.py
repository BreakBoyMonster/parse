from django import forms
from subscribers.models import Subscriber
from scraping.models import Speciality, City


class SubscriberModelForm(forms.ModelForm):
    email = forms.EmailField(label='Email', required=True, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    city = forms.ModelChoiceField(label='City', queryset=City.objects.all(),
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    speciality = forms.ModelChoiceField(label='Speciality', queryset=Speciality.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=100, label='Password',
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Subscriber
        fields = ('email', 'city', 'speciality', 'password')
        exclude = ('is_active',)


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', required=True, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=100, label='Password',
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_password(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email and password:
            qs = Subscriber.objects.filter(email=email).first()
            if qs is None:
                raise forms.ValidationError("User with this email doesn't exist")
            elif password != qs.password:
                raise forms.ValidationError("Wrong password")
        return email


class SubscriberHiddenEmailForm(forms.ModelForm):
    city = forms.ModelChoiceField(label='City', queryset=City.objects.all(),
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    speciality = forms.ModelChoiceField(label='Speciality', queryset=Speciality.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=100, label='Password',
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.HiddenInput())
    is_active = forms.BooleanField(label='Get mailing?', required=False, widget=forms.CheckboxInput)

    class Meta:
        model = Subscriber
        fields = ('email', 'city', 'speciality', 'password', 'is_active')


class ContactForm(forms.Form):
    email = forms.EmailField(label='Email', required=True, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    city = forms.CharField(label='City', max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    speciality = forms.CharField(label='Speciality', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
