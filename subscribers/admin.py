from django.contrib import admin
from subscribers.models import Subscriber


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('email', 'city', 'speciality', 'is_active')
    list_editable = ('is_active',)
