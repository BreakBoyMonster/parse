from django.shortcuts import render, redirect, get_object_or_404
from subscribers.forms import SubscriberModelForm, LoginForm, SubscriberHiddenEmailForm, ContactForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib import messages
from subscribers.models import Subscriber
from find_it.secret import MAILGUN_KEY, API, ADMIN_EMAIL
import requests


class SubscriberCreate(CreateView):
    model = Subscriber
    form_class = SubscriberModelForm
    template_name = 'subscribers/create.html'
    success_url = reverse_lazy('create')

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.success(request, 'Data successfully saved.')
            return self.form_valid(form)
        else:
            messages.error(request, 'Error. Check the data!')
            return self.form_invalid(form)


def login_subscriber(request):
    if request.method == "GET":
        form = LoginForm
        return render(request, 'subscribers/login.html', {'form': form})
    elif request.method == "POST":
        form = LoginForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            request.session['email'] = data['email']
            return redirect('update')
        return render(request, 'subscribers/login.html', {'form': form})


def update_subscriber(request):
    if request.method == "GET" and request.session.get('email', False):
        email = request.session.get('email')
        qs = Subscriber.objects.filter(email=email).first()
        form = SubscriberHiddenEmailForm(initial={'email': qs.email, 'city': qs.city,
                                                  'speciality': qs.speciality, 'password': qs.password,
                                                  'is_active': qs.is_active})
        return render(request, 'subscribers/update.html', {'form': form})
    elif request.method == "POST":
        email = request.session.get('email')
        user = get_object_or_404(Subscriber, email=email)
        form = SubscriberHiddenEmailForm(request.POST or None, instance=user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Data successfully saved.')
            del request.session['email']
            return redirect('list')
        messages.error(request, 'Error. Check the data!')
        return render(request, 'subscribers/update.html', {'form': form})
    else:
        return redirect('login')


def contact_admin(request):
    if request.method == 'POST':
        form = ContactForm(request.POST or None)
        if form.is_valid():
            city = form.cleaned_data['city']
            speciality = form.cleaned_data['speciality']
            from_email = form.cleaned_data['email']
            content = 'Please, add {} city in the search'.format(city)
            content += ', speciality {}. '.format(speciality)
            content += 'Request from user {}'.format(from_email)
            subject = 'Addition to database request'
            requests.post(API, auth=('api', MAILGUN_KEY),
                          data={'from': from_email, 'to': ADMIN_EMAIL, 'subject': subject,
                                'text': content})
            messages.success(request, 'Your mail was sent successfully.')
            return redirect('home')
        return render(request, 'subscribers/contact.html', {'form': form})
    else:
        form = ContactForm
    return render(request, 'subscribers/contact.html', {'form': form})
