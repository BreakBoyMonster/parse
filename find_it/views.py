from django.db import IntegrityError
from django.shortcuts import render, redirect
from scraping.utils import *
from scraping.models import *
import datetime


def parse_new(request):
    city = City.objects.get(name='Kyiv')
    speciality = Speciality.objects.get(name='Python developer')
    urls_qs = Url.objects.filter(city=city, speciality=speciality)
    site = Site.objects.all()
    url_w = urls_qs.get(site=site.get(name='work.ua')).url_address
    url_dj = urls_qs.get(site=site.get(name='djinni.co')).url_address
    # url_dj_odessa = urls_qs.get(site=site.get(name='djinni.co')).url_address
    url_r = urls_qs.get(site=site.get(name='rabota.ua')).url_address
    url_dou = urls_qs.get(site=site.get(name='jobs.dou.ua')).url_address
    jobs = []
    # jobs.extend((djinni(url_dj_odessa)))
    jobs.extend(djinni(url_dj)[0])
    jobs.extend(rabota(url_r)[0])
    jobs.extend(work(url_w)[0])
    jobs.extend(dou(url_dou)[0])

    # v = Vacancy.objects.filter(city=city.pk, speciality=speciality.pk).values('url')
    # url_list = [i['url'] for i in v]
    for job in jobs:
        # print(jobs)
        # print('-' * 10)
        vacancy = Vacancy(city=city, speciality=speciality, url=job['href'],
                          title=job['title'], description=job['description'], company=job['company'])
        try:
            vacancy.save()
        except IntegrityError:
            pass
    # print(len(jobs))
    return redirect('home')
