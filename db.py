import json

import psycopg2
import logging
import datetime
from scraping.utils import *
from find_it.secret import DB_HOST, DB_USER, DB_PASSWORD, DB_NAME

UTILS_FUNC = [(djinni, 'djinni.co'), (work, 'work.ua'), (rabota, 'rabota.ua'), (dou, 'jobs.dou.ua')]

today = datetime.date.today()
ten_days_ago = datetime.date.today() - datetime.timedelta(10)
try:
    conn = psycopg2.connect(dbname=DB_NAME, host=DB_HOST, user=DB_USER, password=DB_PASSWORD)
except:
    logging.exception('Unable to open DataBase - {}'.format(today))
else:
    cur = conn.cursor()
    q = """SELECT city_id, speciality_id FROM subscribers_subscriber WHERE is_active='True';"""
    cur.execute(q)
    cities_qs = cur.fetchall()
    # print(cities_qs)
    todo_list = {i[0]: set() for i in cities_qs}
    for i in cities_qs:
        todo_list[i[0]].add(i[1])
    # print(todo_list)
    all_sites = """SELECT * FROM scraping_site;"""
    cur.execute(all_sites)
    sites_qs = cur.fetchall()
    sites = {i[0]: i[1] for i in sites_qs}
    # print(sites)
    url_list = []
    for city in todo_list:
        for sp in todo_list[city]:
            tmp = dict()
            u = """SELECT site_id, url_address FROM scraping_url WHERE city_id={} AND speciality_id={};""".format(city,
                                                                                                                  sp)
            cur.execute(u)
            qs = cur.fetchall()
            if qs:
                tmp['city'] = city
                tmp['speciality'] = sp
                for item in qs:
                    site_id = item[0]
                    tmp[sites[site_id]] = item[1]
                url_list.append(tmp)
    all_data = []
    errors = []
    if url_list:
        for url in url_list:
            tmp = dict()
            tmp_content = []
            for (func, key) in UTILS_FUNC:
                j, e = func(url[key])
                tmp_content.extend(j)
                errors.extend(e)
            tmp['city'] = url['city']
            tmp['speciality'] = url['speciality']
            tmp['content'] = tmp_content
            all_data.append(tmp)
    if all_data:
        for data in all_data:
            city = data['city']
            speciality = data['speciality']
            jobs = data['content']
            for job in jobs:
                cur.execute("""SELECT * FROM scraping_vacancy WHERE url=%s;""", (job['href'],))
                qs = cur.fetchone()
                if not qs:
                    cur.execute(
                        """INSERT INTO scraping_vacancy (city_id, speciality_id, title, url, description, company, time_stamp) VALUES (%s, %s, %s, %s, %s, %s, %s)""",
                        (city, speciality, job['title'], job['href'], job['description'], job['company'], today))
                    # ins = f"""INSERT INTO scraping_vacancy (city_id, speciality_id, title, url, description, company, time_stamp) VALUES ("{city}", "{speciality}", "{job['title']}", "{job['href']}", "{job['description']}", "{job['company']}", {today});"""
                    # cur.execute(ins)
    if errors:
        cur.execute("""SELECT data FROM scraping_error WHERE time_stamp=%s;""", (today, ))
        err_qs = cur.fetchone()
        if err_qs:
            data = err_qs[0]
            data['errors'].extend(errors)
            cur.execute("""UPDATE scraping_error SET data=%s WHERE time_stamp=%s;""", (json.dumps(data), today, ))
        else:
            data = dict()
            data['errors'] = errors
            cur.execute(
                """INSERT INTO scraping_error (data, time_stamp) VALUES (%s, %s)""",
                (json.dumps(data), today))

    cur.execute("""DELETE FROM scraping_vacancy WHERE time_stamp<=%s;""", (ten_days_ago,))
    conn.commit()
    cur.close()
    conn.close()
