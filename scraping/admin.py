from django.contrib import admin
from scraping.models import *

admin.site.register(City)
admin.site.register(Speciality)
admin.site.register(Url)
admin.site.register(Site)
admin.site.register(Error)


@admin.register(Vacancy)
class VacancyAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', 'url', 'company', 'speciality', 'description', 'time_stamp')
