from django.db import models
from django.contrib.postgres.fields import JSONField


class City(models.Model):
    name = models.CharField(max_length=60, verbose_name='City')

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name


class Speciality(models.Model):
    name = models.CharField(max_length=60, verbose_name='Speciality')

    class Meta:
        verbose_name = 'Speciality'
        verbose_name_plural = 'Specialities'

    def __str__(self):
        return self.name


class Vacancy(models.Model):
    url = models.CharField(max_length=250, unique=True, verbose_name='Vacancy url address')
    title = models.CharField(max_length=250, verbose_name='Vacancy title')
    description = models.TextField(blank=True, verbose_name='Vacancy description')
    company = models.CharField(max_length=250, blank=True, verbose_name='Company name')
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='City')
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, verbose_name='Speciality')
    time_stamp = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Vacancy'
        verbose_name_plural = 'Vacancies'


class Site(models.Model):
    name = models.CharField(max_length=100, verbose_name='Search address')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Site'
        verbose_name_plural = 'Sites'


class Url(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='City')
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, verbose_name='Speciality')
    site = models.ForeignKey(Site, on_delete=models.CASCADE, verbose_name='Search website')
    url_address = models.CharField(max_length=250, unique=True, verbose_name='Search address')

    def __str__(self):
        return 'Speciality {} in {} on website {}'.format(self.speciality, self.city, self.site)

    class Meta:
        verbose_name = 'Search address'
        verbose_name_plural = 'Search addresses'


class Error(models.Model):
    data = JSONField()
    time_stamp = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = 'Scraping error'
        verbose_name_plural = 'Scraping errors'
        ordering = ['-time_stamp']

    def __str__(self):
        return self.time_stamp.strftime('%Y-%m-%d')
