import requests
from bs4 import BeautifulSoup as BS
import datetime
import re
import time
from django.contrib import messages
import random

headers = [{'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36}',
           'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
            {'User-Agent':
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
            'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
            {'User-Agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
            'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
            {'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
            {'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41',
            'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
           ]


pattern = r'\s{2,}|\xa0|\u2060'
pattern_to_get_company = re.compile(r'/jobs/.+/')


def djinni(base_url):
    jobs = []
    urls = []
    errors = []
    cnt = 1
    session = requests.session()
    domain = 'https://djinni.co'
    while cnt < 10:
        # time.sleep(2)
        if base_url:
            urls.append(base_url + '?page=' + str(cnt))
            cnt += 1
            nmb = random.randint(0, 4)
            req = session.get(urls[-1], headers=headers[nmb])
        if req.status_code == 200:
            bsObj = BS(req.content, 'html.parser')
            li_list = bsObj.find_all('li', attrs={'class': 'list-jobs__item'})
            if li_list:
                for li in li_list:
                    div = li.find('div', attrs={'class': 'list-jobs__title'})
                    title = div.a.text
                    href = div.a['href']

                    company = 'No name'
                    comp_div = li.find('div', attrs={'class': 'list-jobs__details__info'})
                    all_a = comp_div.find_all('a')
                    for a in all_a:
                        if pattern_to_get_company.search(a['href']):
                            if a.text:
                                company = a.text
                                break
                    # print(company)

                    short = 'No description'
                    descr = li.find('div', attrs={'class': 'list-jobs__description'})
                    if descr:
                        try:
                            short = descr.p.text
                        except AttributeError:
                            pass
                    jobs.append({'title': title,
                                 'href': domain + href,
                                 'description': short,
                                 'company': company})
            else:
                errors.append({'title': 'The page is empty.',
                               'href': urls[-1]})
                return jobs, errors
        else:
            errors.append({'title': 'Page do not response.',
                           'href': urls[-1]})
            return jobs, errors
    return jobs, errors


# url = 'https://djinni.co/assasas'
# djinni(url)

def rabota(base_url):
    jobs = []
    urls = []
    errors = []
    cnt = 1
    session = requests.session()
    domain = 'https://rabota.ua'
    yesterday = datetime.date.today() - datetime.timedelta(1)
    one_day_ago = yesterday.strftime('%d.%m.%Y')
    urls.append(base_url + '?period=2&lastdate=' + one_day_ago)
    # while True:
    while cnt < 10:
        # time.sleep(2)
        if base_url:
            if cnt > 1:
                urls.append(base_url + '/pg' + str(cnt) + '?period=2&lastdate=' + one_day_ago)
            cnt += 1
            nmb = random.randint(0, 4)
            req = session.get(urls[-1], headers=headers[nmb])
        if req.status_code == 200:
            bsObj = BS(req.content, 'html.parser')
            div_list = bsObj.find_all('div', attrs={'class': 'card-body'})
            if div_list:
                for div in div_list:
                    title = div.find('a', attrs={'class': 'ga_listing'}).text
                    href = div.find('a', attrs={'class': 'ga_listing'})['href']
                    short = 'No description'
                    a_short = div.find('div', attrs={'class': 'card-description'}).text
                    if a_short:
                        short = a_short
                        short = re.sub(pattern, ' ', short)
                        short = short.strip()
                    company = 'No name'
                    a_company = div.find('a', attrs={'class': 'company-profile-name'})
                    if a_company:
                        company = a_company.text
                    jobs.append({'title': title,
                                 'href': domain + href,
                                 'description': short,
                                 'company': company})
            else:
                errors.append({'title': 'The page is empty.',
                               'href': urls[-1]})
                return jobs, errors
        else:
            errors.append({'title': 'Page do not response.',
                           'href': urls[-1]})
            return jobs, errors
    return jobs, errors


def work(base_url):
    jobs = []
    urls = []
    errors = []
    cnt = 1
    session = requests.session()
    domain = 'https://www.work.ua'
    # while True:
    while cnt < 10:
        # time.sleep(2)
        if base_url:
            urls.append(base_url + '?page=' + str(cnt))
            cnt += 1
            nmb = random.randint(0, 4)
            req = session.get(urls[-1], headers=headers[nmb])
        if req.status_code == 200:
            bsObj = BS(req.content, 'html.parser')
            div_list = bsObj.find_all('div', attrs={'class': 'job-link'})
            if div_list:
                for div in div_list:
                    title = div.find('a').text
                    href = div.a['href']
                    short = div.p.text
                    short = re.sub(pattern, ' ', short)
                    short = short.strip()
                    span_list = div.find_all('span')
                    company = 'No name'
                    for span in span_list:
                        b = span.find('b')
                        if b:
                            company = b.text
                            break
                    jobs.append({'title': title,
                                 'href': domain + href,
                                 'description': short,
                                 'company': company})
            else:
                errors.append({'title': 'The page is empty.',
                               'href': urls[-1]})
                return jobs, errors
        else:
            errors.append({'title': 'Page do not response.',
                           'href': urls[-1]})
            return jobs, errors
    return jobs, errors


def dou(base_url):
    jobs = []
    errors = []
    if base_url:
        session = requests.session()
        # time.sleep(2)
        nmb = random.randint(0, 4)
        req = session.get(base_url, headers=headers[nmb])
    if req.status_code == 200:
        bsObj = BS(req.content, 'html.parser')
        div_list = bsObj.find_all('div', attrs={'class': 'vacancy'})
        if div_list:
            for div in div_list:
                title = div.find('a', attrs={'class': 'vt'}).text
                href = div.find('a', attrs={'class': 'vt'})['href']
                company = "No company"
                short = "No short"
                a_company = div.find('a', attrs={'class': 'company'}).text
                if a_company:
                    company = a_company
                a_short = div.find('div', attrs={'class': 'sh-info'}).text
                if a_short:
                    short = a_short
                    short = re.sub(pattern, ' ', short)
                    short = short.strip()
                jobs.append({'title': title,
                             'href': href,
                             'description': short,
                             'company': company})
        else:
            errors.append({'title': 'The page is empty.',
                           'href': base_url})
            return jobs, errors
    else:
        errors.append({'title': 'The page is empty.',
                       'href': base_url})
        return jobs, errors
    return jobs, errors
