import requests
from bs4 import BeautifulSoup as BS
import re

session = requests.session()
headers = {'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36}',
           'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
           }
base_url = 'https://djinni.co/jobs/keyword-python/kyiv/'
domain = 'https://djinni.co'
jobs = []
urls = []
pattern = r'\s{2,}|\xa0|\u2060'


def get_company(base_url, cnt):

    urls.append(base_url + '?page=' + str(cnt))
    req = session.get(urls[-1], headers=headers)

    if req.status_code == 200:
        bsObj = BS(req.content, 'html.parser')
        li_list = bsObj.find_all('li', attrs={'class': 'list-jobs__item'})
        for li in li_list:
            div = li.find('div', attrs={'class': 'list-jobs__title'})
            title = div.a.text
            href = div.a['href']
            short = 'No description'
            descr = li.find('div', attrs={'class': 'list-jobs__description'})
            if descr:
                short = descr.p.text
            jobs.append({'title': title,
                         'href': domain + href,
                         'description': short,
                         'company': 'No name'})

        if li_list:
            print(cnt)
            return True
    return


cnt = 1
while cnt < 11:
    if not get_company(base_url, cnt):
        break
    cnt += 1
# while get_company(base_url, cnt):
#     cnt += 1

template = '<!doctype html><html lang="en">\n<head><meta charset="utf-8"></head>\n<body>'
end = '\n</body>\n</html>'
content = '\n<h2>Djinni.co</h2>'
for job in jobs:
    content += '''\n\t<a href="{href}" target="_blank">{title}</a><br />
                  \n\t<p>{description}</p>\n\t<p>{company}</p>\n<br />'''.format(**job)
    content += '<hr><br /><br />'
data = template + content + end

handle = open('jobs.html', 'w', encoding='utf-8')
handle.write(str(data))
handle.close()

# for i in jobs:
#     print(i)
