import requests
from bs4 import BeautifulSoup as BS
# import codecs
import time
import re
import datetime

session = requests.session()
headers = {'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36}',
           'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
           }
base_url = 'https://rabota.ua/zapros/python-developer/%D1%83%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0/'
domain = 'https://rabota.ua'
jobs = []
urls = []
pattern = r'\s{2,}|\xa0|\u2060'
yesterday = datetime.date.today()-datetime.timedelta(1)
one_day_ago = yesterday.strftime('%d.%m.%Y')


def get_company(base_url, cnt):
    if cnt > 1:
        urls.append(base_url + 'pg' + str(cnt) + '?period=2&lastdate=' + one_day_ago)
    else:
        urls.append(base_url + '?period=2&lastdate=' + one_day_ago)
    req = session.get(urls[-1], headers=headers)

    if req.status_code == 200:
        bsObj = BS(req.content, 'html.parser')
        div_list = bsObj.find_all('div', attrs={'class': 'card-body'})
        for div in div_list:
            title = div.find('a', attrs={'class': 'ga_listing'}).text
            href = div.find('a', attrs={'class': 'ga_listing'})['href']
            short = 'No description'
            a_short = div.find('div', attrs={'class': 'card-description'}).text
            if a_short:
                short = a_short
                short = re.sub(pattern, ' ', short)
                short = short.strip()
            company = 'No name'
            a_company = div.find('a', attrs={'class': 'company-profile-name'}).text
            if a_company:
                company = a_company

            jobs.append({'title': title,
                         'href': domain + href,
                         'description': short,
                         'company': company})

        if div_list:
            return True
    return


cnt = 1
while get_company(base_url, cnt):
    cnt += 1

template = '<!doctype html><html lang="en">\n<head><meta charset="utf-8"></head>\n<body>'
end = '\n</body>\n</html>'
content = '\n<h2>Rabota.ua</h2>'
print(len(jobs))
for job in jobs:
    content += '\n\t<a href="{href}" target="_blank">{title}</a><br />\n\t<p>{description}</p>\n\t<p>{company}</p>\n<br />'.format(**job)
    content += '<hr><br /><br />'
data = template + content + end

handle = open('jobs.html', 'w', encoding='utf-8')
handle.write(str(data))
handle.close()
